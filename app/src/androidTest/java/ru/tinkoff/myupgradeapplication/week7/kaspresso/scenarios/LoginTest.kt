package ru.tinkoff.myupgradeapplication.week7.kaspresso.scenarios

import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.week7.kaspresso.kaspressoBuilder
import ru.tinkoff.myupgradeapplication.week7.kaspresso.screen.LoginScreen
import ru.tinkoff.myupgradeapplication.week7.kaspresso.screen.MainScreen

class LoginTest : TestCase(kaspressoBuilder) {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun emptyPasswordTest() = run {
        step("Нажать на кнопку Next") {
            MainScreen().nextButton.click()
        }
        step("Ввести текст в поле login") {
            LoginScreen().loginTextView.typeText("Tinkoff")
        }
        step("Нажать submit и проверить предупреждение") {
            LoginScreen {
                submitButton.click()

                snackBar.hasText("Password field must be filled!")
            }
        }
    }

    @Test
    fun emptyLoginTest() = run {
        step("Нажать на кнопку Next") {
            MainScreen().nextButton.click()
        }
        step("Ввести текст в поле password") {
            LoginScreen().passwordTextView.typeText("Upgrade")
        }
        step("Нажать submit и проверить предупреждение") {
            LoginScreen {
                submitButton.click()

                snackBar.hasText("Login field must be filled!")
            }
        }
    }

    @Test
    fun emptyFieldsTest() = run {
        step("Нажать на кнопку Next") {
            MainScreen().nextButton.click()
        }
        step("Нажать submit и проверить предупреждение") {
            LoginScreen {
                submitButton.click()

                snackBar.hasText("Both of fields must be filled!")
            }
        }
    }

    @Test
    fun checkFieldsStateAfterReturnPageTest() = run {
        step("Нажать на кнопку Next") {
            MainScreen().nextButton.click()
        }
        step("Заполнить все поля и вернуться на главный экран") {
            LoginScreen {
                loginTextView.typeText("Tinkoff")
                passwordTextView.typeText("Upgrade")
                previousButton.click()
            }
        }
        step("Нажать на кнопку Next") {
            MainScreen().nextButton.click()
        }
        step("Нажать submit и проверить предупреждение") {
            LoginScreen().checkFieldsAreEmpty()
        }
    }
}
