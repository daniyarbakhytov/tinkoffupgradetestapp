package ru.tinkoff.myupgradeapplication.week6.screen

import androidx.test.uiautomator.By
import androidx.test.uiautomator.Until
import ru.tinkoff.myupgradeapplication.week6.util.ANDROID_BUNDLE_ID

class HomePage : BasePage(){

    val nextButton = By.res("$ANDROID_BUNDLE_ID:id/button_first")
    val changeButton = By.res("$ANDROID_BUNDLE_ID:id/change_button")
    val dialogButton = By.res("$ANDROID_BUNDLE_ID:id/dialog_button")

    fun goToLoginPage() {
        device
            .wait(Until.findObject(nextButton), waitingTimeout)
            .click()
    }

    fun changeMainText() {
        device
            .wait(Until.findObject(changeButton), waitingTimeout)
            .click()
    }

    fun showDialog() {
        device
            .wait(Until.findObject(dialogButton), waitingTimeout)
            .click()
    }

    fun checkTextOnMainScreen(text: String) {
        assert(device.wait(Until.hasObject(By.text(text)), waitingTimeout))

    }
}