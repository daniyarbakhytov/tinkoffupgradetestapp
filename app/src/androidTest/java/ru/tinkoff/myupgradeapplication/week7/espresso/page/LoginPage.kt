package ru.tinkoff.myupgradeapplication.week7.espresso.page

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withParent
import com.google.android.material.snackbar.SnackbarContentLayout
import com.google.android.material.textview.MaterialTextView
import org.hamcrest.Matchers
import ru.tinkoff.myupgradeapplication.R

class LoginPage : BasePage() {

    private val loginTextView = onView(withId(R.id.edittext_login))
    private val passwordTextView = onView(withId(R.id.edittext_password))
    private val submitButton = onView(withId(R.id.button_submit))
    private val previousButton = onView(withId(R.id.button_second))
    private val snackBar = onView(
        Matchers.allOf(
            Matchers.instanceOf(MaterialTextView::class.java),
            withParent(Matchers.instanceOf(SnackbarContentLayout::class.java))
        )
    )

    fun enterLogin(login: String) {
        loginTextView.enterText(login)
    }

    fun enterPassword(password: String) {
        passwordTextView.enterText(password)
    }

    fun submit() {
        submitButton.clickButton()
    }

    fun goToPreviousPage() {
        previousButton.clickButton()
    }

    fun checkFieldsAreEmpty() {
        val emptyFieldError = "Both of fields must be filled!"

        loginTextView.checkFieldIsEmpty()
        passwordTextView.checkFieldIsEmpty()

        submit()
        checkErrorMessage(emptyFieldError)
    }

    fun checkErrorMessage(errorMessage: String) {
        snackBar.checkText(errorMessage)
    }

    companion object {
        inline operator fun invoke(crossinline block: LoginPage.() -> Unit) =
            LoginPage().block()
    }
}
