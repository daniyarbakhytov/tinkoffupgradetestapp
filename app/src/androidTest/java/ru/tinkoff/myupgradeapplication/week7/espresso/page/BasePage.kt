package ru.tinkoff.myupgradeapplication.week7.espresso.page

import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText

open class BasePage {
    fun ViewInteraction.clickButton() {
        this.perform(click())
    }

    fun ViewInteraction.enterText(text: String) {
        this.perform(typeText(text), closeSoftKeyboard())
    }

    fun ViewInteraction.checkText(text: String) {
        this.check(matches(withText(text)))
    }

    fun ViewInteraction.checkFieldIsEmpty() {
        this.check(matches(withText("")))
    }

    fun ViewInteraction.isElementDisplayed() {
        this.check(matches(isDisplayed()))
    }

    fun ViewInteraction.isNotDisplayed() {
        this.check(doesNotExist())
    }
}
