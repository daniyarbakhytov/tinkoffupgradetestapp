package ru.tinkoff.myupgradeapplication.week6.screen

import androidx.test.uiautomator.By
import androidx.test.uiautomator.BySelector
import androidx.test.uiautomator.Until

class DialogPage : BasePage() {

    val dialogTitle = By.text("Важное сообщение")
    val dialogDescription = By.text("Теперь ты автоматизатор")

    fun dialogIsDisplayed() {
        assert(waitForElement(dialogTitle))
        assert(waitForElement(dialogDescription))
    }

    fun dialogIsNotDisplayed() {
        assert(!waitForElement(dialogTitle))
        assert(!waitForElement(dialogDescription))
    }

    private fun waitForElement(selector: BySelector): Boolean {
        return device.wait(Until.hasObject(selector), waitingTimeout)
    }
}