package ru.tinkoff.myupgradeapplication.week7.kaspresso.scenarios

import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.kaspersky.kaspresso.internal.systemscreen.DataUsageSettingsScreen.pressBack
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.week7.kaspresso.kaspressoBuilder
import ru.tinkoff.myupgradeapplication.week7.kaspresso.screen.DialogScreen
import ru.tinkoff.myupgradeapplication.week7.kaspresso.screen.MainScreen

class DialogTestLoginTest : TestCase(kaspressoBuilder) {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkDialogTest() = run {
        step("Нажать на кнопку Show Dialog") {
            MainScreen().dialogButton.click()
        }
        step("Проверить что диалог отображается") {
            DialogScreen().dialogIsDisplayed()
        }
    }

    @Test
    fun checkDialogIsNotDisplayedTest() = run {
        step("Нажать на кнопку Show Dialog и скрыть его") {
            MainScreen().dialogButton.click()
            pressBack()
        }
        step("Проверить что диалог не отображается") {
            DialogScreen().dialogIsNotDisplayed()
        }
    }
}
