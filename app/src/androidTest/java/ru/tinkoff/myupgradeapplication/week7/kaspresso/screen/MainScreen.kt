package ru.tinkoff.myupgradeapplication.week7.kaspresso.screen

import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import ru.tinkoff.myupgradeapplication.R

class MainScreen : BaseScreen() {

    val nextButton = KButton { withId(R.id.button_first) }
    val changeButton = KButton { withId(R.id.change_button) }
    val dialogButton = KButton { withId(R.id.dialog_button) }
    val mainTextView = KTextView { withId(R.id.textview_first) }

    companion object {
        inline operator fun invoke(crossinline block: MainScreen.() -> Unit) =
            MainScreen().block()
    }
}
