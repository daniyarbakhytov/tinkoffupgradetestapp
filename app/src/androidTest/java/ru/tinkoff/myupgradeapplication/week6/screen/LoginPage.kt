package ru.tinkoff.myupgradeapplication.week6.screen

import androidx.test.uiautomator.By
import androidx.test.uiautomator.Until
import ru.tinkoff.myupgradeapplication.week6.util.ANDROID_BUNDLE_ID

class LoginPage : BasePage() {

    val previousButton = By.res("$ANDROID_BUNDLE_ID:id/button_second")
    val loginEditText = By.res("$ANDROID_BUNDLE_ID:id/edittext_login")
    val passwordEditText = By.res("$ANDROID_BUNDLE_ID:id/edittext_password")
    val submitButton = By.res("$ANDROID_BUNDLE_ID:id/button_submit")

    fun enterLogin(loginValue: String) {
        device
            .wait(Until.findObject(loginEditText), waitingTimeout)
            .text = loginValue
    }

    fun enterPassword(passwordValue: String) {
        device
            .wait(Until.findObject(passwordEditText), waitingTimeout)
            .text = passwordValue
    }

    fun returnToMainPage() {
        device
            .wait(Until.findObject(previousButton), waitingTimeout)
            .click()
    }

    fun submitData() {
        device
            .wait(Until.findObject(submitButton), waitingTimeout)
            .click()
    }

    fun checkEmptyFieldErrorMessage(errorMessage: String) {
        assert(device.wait(Until.hasObject(By.text(errorMessage)), waitingTimeout))
    }
}