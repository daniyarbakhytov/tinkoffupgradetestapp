package ru.tinkoff.myupgradeapplication.week7.kaspresso.scenarios

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.R
import ru.tinkoff.myupgradeapplication.week7.kaspresso.kaspressoBuilder
import ru.tinkoff.myupgradeapplication.week7.kaspresso.screen.LoginScreen
import ru.tinkoff.myupgradeapplication.week7.kaspresso.screen.MainScreen

class SwitchTextTest : TestCase(kaspressoBuilder) {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    private val context = InstrumentationRegistry.getInstrumentation().targetContext

    @Test
    fun textStateAfterReturnToPageTest() = run {
        val firstText = context.getString(R.string.first_text)
        val secondText = context.getString(R.string.second_text)

        step("Поменять текст на главном экране") {
            Thread.sleep(1000L)
            MainScreen {
                changeButton.click()
                mainTextView.hasText(firstText)
            }
        }
        step("Инсцинировать навигацию") {
            MainScreen().nextButton
            LoginScreen().previousButton
        }
        step("Проерить что отображается изначальный текст") {
            MainScreen().mainTextView.hasText(secondText)
        }
    }
}
