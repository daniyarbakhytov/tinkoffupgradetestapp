package ru.tinkoff.myupgradeapplication.week7.espresso.scenarios

import androidx.test.espresso.Espresso.pressBack
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.week7.espresso.page.DialogPage
import ru.tinkoff.myupgradeapplication.week7.espresso.page.MainPage

@RunWith(AndroidJUnit4::class)
class DialogTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkDialogTest() {
        MainPage().showDialog()

        DialogPage().dialogIsDisplayed()
    }

    @Test
    fun checkDialogIsNotDisplayedTest() {
        MainPage().showDialog()
        pressBack()

        DialogPage().dialogIsNotDisplayed()
    }
}
