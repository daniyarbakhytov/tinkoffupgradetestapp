package ru.tinkoff.myupgradeapplication.week6.scenarios

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.R
import ru.tinkoff.myupgradeapplication.week6.screen.HomePage
import ru.tinkoff.myupgradeapplication.week6.screen.LoginPage

class SwitchTextTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    val context = InstrumentationRegistry.getInstrumentation().targetContext

    @Test
    fun textStateAfterReturnToPageTest() {
        val firstText = context.getString(R.string.first_text)
        val secondText = context.getString(R.string.second_text)

        with(HomePage()) {
            changeMainText()
            checkTextOnMainScreen(secondText)
            goToLoginPage()
        }
        with(LoginPage()) {
            pressBack()
        }
        with(HomePage()) {
            checkTextOnMainScreen(firstText)
        }
    }
}