package ru.tinkoff.myupgradeapplication.week7.espresso.page

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withText

class DialogPage : BasePage() {
    private val dialogTitle = onView(withText("Важное сообщение"))
    private val dialogDescription = onView(withText("Теперь ты автоматизатор"))

    fun dialogIsDisplayed() {
        dialogTitle.isElementDisplayed()
        dialogDescription.isElementDisplayed()
    }

    fun dialogIsNotDisplayed() {
        dialogTitle.isNotDisplayed()
        dialogDescription.isNotDisplayed()
    }
}
