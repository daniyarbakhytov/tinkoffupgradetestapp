package ru.tinkoff.myupgradeapplication.week7.espresso.scenarios

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.R
import ru.tinkoff.myupgradeapplication.week7.espresso.page.LoginPage
import ru.tinkoff.myupgradeapplication.week7.espresso.page.MainPage

@RunWith(AndroidJUnit4::class)
class SwitchTextTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    private val context = InstrumentationRegistry.getInstrumentation().targetContext

    @Test
    fun textStateAfterReturnToPageTest() {
        val firstText = context.getString(R.string.first_text)
        val secondText = context.getString(R.string.second_text)

        MainPage() {
            changeText()
            checkTextOnMainPage(secondText)
            goToLoginPage()
        }
        LoginPage().goToPreviousPage()

        MainPage().checkTextOnMainPage(firstText)
    }
}
