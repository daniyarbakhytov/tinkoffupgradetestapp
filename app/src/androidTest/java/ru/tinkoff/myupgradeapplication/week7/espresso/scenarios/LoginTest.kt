package ru.tinkoff.myupgradeapplication.week7.espresso.scenarios

import androidx.appcompat.widget.AppCompatTextView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withParent
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.snackbar.SnackbarContentLayout
import com.google.android.material.textview.MaterialTextView
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.instanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.R
import ru.tinkoff.myupgradeapplication.week7.espresso.page.LoginPage
import ru.tinkoff.myupgradeapplication.week7.espresso.page.MainPage

@RunWith(AndroidJUnit4::class)
class LoginTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun emptyPasswordTest() {
        val login = "Tinkoff"
        val passwordError = "Password field must be filled!"

        MainPage().goToLoginPage()
        LoginPage() {
            enterLogin(login)
            submit()
            checkErrorMessage(passwordError)
        }

        onView(withId(R.id.edittext_login))
            .perform(typeText(""))

        onView(
            allOf(
                instanceOf(AppCompatTextView::class.java),
                withParent(instanceOf(MaterialToolbar::class.java))))
            .check(matches(withText("First Fragment")))

        onView(withId(R.id.fab))
            .perform(click())

        onView(
            allOf(
                instanceOf(MaterialTextView::class.java),
                withParent(instanceOf(SnackbarContentLayout::class.java))))
            .check(matches(withText("Replace with your own action")))
    }

    @Test
    fun emptyLoginTest() {
        val password = "Upgrade"
        val loginError = "Login field must be filled!"

        MainPage().goToLoginPage()
        LoginPage() {
            enterPassword(password)
            submit()
            checkErrorMessage(loginError)
        }
    }

    @Test
    fun emptyFieldsTest() {
        val emptyFieldError = "Both of fields must be filled!"

        MainPage().goToLoginPage()
        LoginPage() {
            submit()
            checkErrorMessage(emptyFieldError)
        }
    }

    @Test
    fun checkFieldsStateAfterReturnPageTest() {
        val login = "Tinkoff"
        val password = "Upgrade"

        MainPage().goToLoginPage()
        LoginPage() {
            enterLogin(login)
            enterPassword(password)
            goToPreviousPage()
        }
        MainPage().goToLoginPage()
        LoginPage().checkFieldsAreEmpty()
    }
}
