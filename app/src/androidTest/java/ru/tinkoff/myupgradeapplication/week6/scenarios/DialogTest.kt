package ru.tinkoff.myupgradeapplication.week6.scenarios

import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.week6.screen.DialogPage
import ru.tinkoff.myupgradeapplication.week6.screen.HomePage

class DialogTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkDialogTest() {
        with(HomePage()) {
            showDialog()
        }
        with(DialogPage()) {
            dialogIsDisplayed()
        }
    }

    @Test
    fun checkDialogIsNotVisibleTest() {
        with(HomePage()) {
            showDialog()
        }
        with(DialogPage()) {
            dialogIsDisplayed()
            pressBack()
            dialogIsNotDisplayed()
        }
    }
}