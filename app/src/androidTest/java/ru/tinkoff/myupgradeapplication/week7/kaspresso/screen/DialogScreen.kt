package ru.tinkoff.myupgradeapplication.week7.kaspresso.screen

import io.github.kakaocup.kakao.text.KTextView

class DialogScreen : BaseScreen() {
    private val dialogTitle = KTextView { withText("Важное сообщение") }
    private val dialogDescription = KTextView { withText("Теперь ты автоматизатор") }

    fun dialogIsDisplayed() {
        dialogTitle.isDisplayed()
        dialogDescription.isDisplayed()
    }

    fun dialogIsNotDisplayed() {
        dialogTitle.doesNotExist()
        dialogDescription.doesNotExist()
    }
}
