package ru.tinkoff.myupgradeapplication.week6.screen

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice

open class BasePage {
    val device: UiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    val waitingTimeout = 2000L

    fun pressBack() {
        device.pressBack()
    }
}