package ru.tinkoff.myupgradeapplication.week7.espresso.page

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers
import ru.tinkoff.myupgradeapplication.R

class MainPage : BasePage() {
    private val nextButton = onView(ViewMatchers.withId(R.id.button_first))
    private val changeButton = onView(ViewMatchers.withId(R.id.change_button))
    private val mainTextView = onView(ViewMatchers.withId(R.id.textview_first))
    private val showDialogButton = onView(ViewMatchers.withId(R.id.dialog_button))

    fun goToLoginPage() {
        nextButton.clickButton()
    }

    fun showDialog() {
        showDialogButton.clickButton()
    }

    fun changeText() {
        changeButton.clickButton()
    }
    fun checkTextOnMainPage(text: String) {
        mainTextView.checkText(text)
    }

    companion object {
        inline operator fun invoke(crossinline block: MainPage.() -> Unit) =
            MainPage().block()
    }
}
