package ru.tinkoff.myupgradeapplication.week6.scenarios

import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.week6.screen.HomePage
import ru.tinkoff.myupgradeapplication.week6.screen.LoginPage

class LoginTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun emptyPasswordErrorTest() {
        val login = "Tinkoff"
        val passwordError = "Password field must be filled!"

        with(HomePage()) {
            goToLoginPage()
        }
        with(LoginPage()) {
            enterLogin(login)
            submitData()

            checkEmptyFieldErrorMessage(passwordError)
        }
    }

    @Test
    fun emptyLoginErrorTest() {
        val password = "Upgrade"
        val loginError = "Login field must be filled!"

        with(HomePage()) {
            goToLoginPage()
        }
        with(LoginPage()) {
            enterPassword(password)
            submitData()

            checkEmptyFieldErrorMessage(loginError)
        }
    }

    @Test
    fun emptyFieldsErrorTest() {
        val emptyFieldError = "Both of fields must be filled!"

        with(HomePage()) {
            goToLoginPage()
        }
        with(LoginPage()) {
            submitData()

            checkEmptyFieldErrorMessage(emptyFieldError)
        }
    }

    @Test
    fun checkFiledsStateAfterReturnPageTest() {
        val login = "Tinkoff"
        val password = "Upgrade"
        val emptyFieldError = "Both of fields must be filled!"

        with(HomePage()) {
            goToLoginPage()
        }
        with(LoginPage()) {
            enterLogin(login)
            enterPassword(password)
            returnToMainPage()
        }
        with(HomePage()) {
            goToLoginPage()
        }
        with(LoginPage()) {
            submitData()

            checkEmptyFieldErrorMessage(emptyFieldError)
        }
    }
}