package ru.tinkoff.myupgradeapplication.week7.kaspresso.screen

import com.google.android.material.snackbar.SnackbarContentLayout
import com.google.android.material.textview.MaterialTextView
import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import ru.tinkoff.myupgradeapplication.R

class LoginScreen : BaseScreen() {
    val loginTextView = KEditText { withId(R.id.edittext_login) }
    val passwordTextView = KEditText { withId(R.id.edittext_password) }
    val submitButton = KButton { withId(R.id.button_submit) }
    val previousButton = KButton { withId(R.id.button_second) }
    val snackBar = KTextView {
        isInstanceOf(MaterialTextView::class.java)
        withParent { isInstanceOf(SnackbarContentLayout::class.java) }
    }

    fun checkFieldsAreEmpty() {
        loginTextView.hasEmptyText()
        passwordTextView.hasEmptyText()
        submitButton.click()

        snackBar.hasText("Both of fields must be filled!")
    }

    companion object {
        inline operator fun invoke(crossinline block: LoginScreen.() -> Unit) =
            LoginScreen().block()
    }
}
